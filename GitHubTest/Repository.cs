﻿using System;

namespace GitHubTest
{
    public class Repository
    {
        public string OwnerName { get; set; }

        public string Name { get; set; }

        public string URL { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime LastPushed { get; set; }
    }
}