﻿using System;

namespace GitHubTest
{
    public class Commit
    {
        public string Repository { get; set; }

        public string Owner { get; set; }

        public string Committer { get; set; }

        public string Message { get; set; }

        public string SHAHash { get; set; }

        public DateTime Date { get; set; }
    }
}