﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GitHubTest
{
    class Program
    {
        static void Main(string[] args)
        {
            MainAsync(args).Wait();
        }

        static async Task MainAsync(string[] args)
        {
            var sourceControlService = new GitHubService();

            var bookATableRepositories = sourceControlService.GetRepositories("Bookatable").Result;
            var commitList = new List<Commit>();

            foreach (var repository in bookATableRepositories)
            {
                commitList.AddRange(sourceControlService.GetCommits("Malcolm Young", repository.Name).Result);
            }

            foreach (var commit in commitList)
            {
                Console.WriteLine($"{commit.Message} , by {commit.Committer}");
            }

            Console.ReadLine();

            /// SOMETHING LIKE THIS IS WHAT WE WOULD LIKE TO SEE IN PAIR PROGRAMMING - understanding of threads, dropping and assigning?

            //var taskList = new List<Task<IEnumerable<Commit>>>();

            //foreach (var repository in bookATableRepositories)
            //{
            //    taskList.Add(sourceControlService.GetCommits("Malcolm Young", repository.Name));
            //}

            //Task.WaitAll(taskList.ToArray());

            //foreach (var task in taskList)
            //{
            //    if (task.IsCompleted)
            //    {
            //        commitList.AddRange(task.Result);
            //    }
            //}
        }
    }
}
