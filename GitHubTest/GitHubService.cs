﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GitHubTest
{
    /// <summary>
    /// Things I feel we should be looking for in the result:
    /// 1. Injecting the GitHubWebClient instead of newing it
    /// 2. Give meaningful variable names instead of acronyms that make sense to noone
    /// 3. Using the using { } code fragment to ensure the GitHubWebClient's dispose method is called. They would have to change the IGitHubWebClient to inherit from IDisposable, instead of GitHubWebClient
    /// inheriting from both IGitHubWebClient and IDisposable, as an example.
    /// 4. IDEALLY moving the .Where methods into their own interface, something like IRepositoryFilter and ICommitFilter, that take a strongly typed filter POCO object as input
    /// and return enumerables of repos and commits appropriately
    /// 5. If not 4, then move the linqs into their own private methods for more readable code.
    /// 6. Add unit tests with well written names, hopefully some basic integration tests as well (a rudimentary end to end on the Github service)
    /// 7. Something that we could ask them in pairing if there is still time after the Program main.cs class and moving it from sync to async : change the
    /// repositories filter to add a : all repos of at least one week of age. We/I would hope to see them add another implementation of the filter interface, rather than
    /// go ahead and modify. For me it helps me see how a developer thinks about looking at code.
    /// </summary>
    public class GitHubService : ISourceControlService
    {
        // Case insensitive
        public async Task<IEnumerable<Repository>> GetRepositories(string searchQuery)
        {
            var ghc = new LegacySourceCodeRepository();
            var rps = await ghc.GetRepositories();

            return rps.Where(x => x.OwnerName == searchQuery);
        }

        public async Task<IEnumerable<Commit>> GetCommits(string owner, string repository)
        {
            var ghc = new LegacySourceCodeRepository();
            var commits = await ghc.GetCommits();

            return commits.Where(x => x.Owner == owner && x.Repository == repository);
        }
    }
}