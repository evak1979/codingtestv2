﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GitHubTest
{
    public interface ISourceControlService
    {
        Task<IEnumerable<Repository>> GetRepositories(string searchQuery);

        Task<IEnumerable<Commit>> GetCommits(string owner, string repository);
    }
}