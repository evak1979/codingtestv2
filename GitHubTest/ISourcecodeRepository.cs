﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GitHubTest
{
    public interface ISourcecodeRepository : IDisposable
    {
        Task<List<Repository>> GetRepositories();

        Task<List<Commit>> GetCommits();
    }
}