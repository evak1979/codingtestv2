﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GitHubTest
{
    /// <summary>
    /// Here we would probably like to see him have IGitHubWebClient inherit from IDisposable
    /// IGitHubWebClient can be modified
    /// GitHubWebClient cannot be modified
    /// </summary>
    public class LegacySourceCodeRepository : ISourcecodeRepository
    {
        private bool ConnectionActive { get; set; }

        public LegacySourceCodeRepository()
        {
            ConnectionActive = true;
        }

        private void CloseConnection()
        {
            ConnectionActive = false;
        }

        public void Dispose()
        {
            CloseConnection();
        }

        public Task<List<Repository>> GetRepositories()
        {
            return Task.FromResult(new List<Repository>
                    {
                        new Repository
                        {
                            CreatedDate = DateTime.Now,
                            LastPushed = DateTime.Now.AddDays(-1),
                            Name = "BookatableRepoOne",
                            OwnerName = "Bookatable",
                            URL = "One vision"
                        },
                        new Repository
                        {
                            CreatedDate = DateTime.Now,
                            LastPushed = DateTime.Now.AddDays(-1),
                            Name = "Second repo",
                            OwnerName = "Dontbookatable",
                            URL = "And repos for all"
                        },
                        new Repository
                        {
                            CreatedDate = DateTime.Now,
                            LastPushed = DateTime.Now.AddDays(-13),
                            Name = "BookatableRepoTwo",
                            OwnerName = "Bookatable",
                            URL = "The number of the table"
                        },
                        new Repository
                        {
                            CreatedDate = DateTime.Now,
                            LastPushed = DateTime.Now.AddDays(-1),
                            Name = "Third repo",
                            OwnerName = "Someothertable",
                            URL = "The number of the table"
                        },
                        new Repository
                        {
                            CreatedDate = DateTime.Now,
                            LastPushed = DateTime.Now.AddDays(-5),
                            Name = "BookatableRepoThree",
                            OwnerName = "Bookatable",
                            URL = "The number of the table"
                        }
                    });
        }

        public Task<List<Commit>> GetCommits()
        {
            return Task.FromResult(new List<Commit>
            {
                new Commit
                {
                    Repository = "BookatableRepoOne",
                    Owner = "Rob Halford",
                    Committer = "Bruce Dickinson",
                    Date = DateTime.Now.AddDays(-1),
                    Message = "Fixed a bug where the code would crush at two minutes to midnight...",
                    SHAHash = "12345"
                },
                new Commit
                {
                    Repository = "BookatableRepoOne",
                    Owner = "Rob Halford",
                    Committer = "Steve Harris",
                    Date = DateTime.Now.AddDays(-2),
                    Message = "Added electric eye functionality to security camera...",
                    SHAHash = "12345"
                },
                new Commit
                {
                    Repository = "BookatableRepoTwo",
                    Owner = "Malcolm Young",
                    Committer = "Angus Young",
                    Date = DateTime.Now.AddDays(-3),
                    Message = "For those about to release to prod... I salute you...",
                    SHAHash = "12345"
                },
                new Commit
                {
                    Repository = "BookatableRepoThree",
                    Owner = "Malcolm Young",
                    Committer = "Yngwie Malmsteen",
                    Date = DateTime.Now.AddDays(-4),
                    Message = "Added black star theme to the UI...",
                    SHAHash = "12345"
                },
            });
        }
    }
}