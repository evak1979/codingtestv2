﻿# Bookatable coding test

A junior software engineer was asked to perform the following task:

Given an legacy piece of code that connects to yet another legacy database, retrieve all repositories owned by Bookatable, and further filter the results by the Owner name. The engineer provided his team with a workable solution, but upon further inspections the team discovered a few quality issues. 

Your task is to refactor the GitHubService class in order to:

  - remove code smells.
  - engineer the code to be scalable and testable.
  - improve the code's performance.
  - include tests for the GitHubService class.

Feel free to introduce new code functionality if you feel there is a need to. Should you do so, we strongly encourage that you add tests to the newly introduced code.

## Things you can change:

Feel free to alter the GitHubService class as well as the IGitHubService as required. You are also welcome to introduce new classes/interfaces if you feel there is a need for it.

## Things you do not need to worry about:
  - You do not need to touch any other code file.
  - You do not need to add tests for the main method or for the GitHubWeb client.
  - You do not need to make any changes to LegacySourceCodeRepository - but you should take into account that it will be replaced by something altogether different in the future.

## When done
We do not expect you to spend more than a couple of hours on this task. When done, create a pull request with your name against the master branch. Include your first and last name, as well as the title of the role you are applying for. 

Thank you and good luck!

## For dev purposes - to be removed in final version:

I have left comments in the code of what we'd expect the candidate to discuss or try and do. The comments on program.cs is basically our existing interview, and we could still ask them to improve in my (intentionally) bad code to make the calls async - I've even written an example of how I'd imagine I'd go about it but please feel free to improve on it.

I've also added comments on the githubservice.cs class on things I feel we should look for when code reviewing their attempt. 